# Order of frequency function

class Array
  def order_of_frequency
    self.inject(Hash.new(0)) { |h, e| h[e] += 1; h }
        .sort_by { |_key, value| -value }.map { |a| a[0] }
  end
end

p ['a', 'b', 'b', 'c', 'b', 'a'].order_of_frequency
