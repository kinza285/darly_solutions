#flatten without monkey patching

p ['a', ['b', 'b'], [['c', ['b']], 'a']].flatten

#flatten with monkey patching

class Array
  def flatten
    array = self.inject([]) do |a, e| 
                              e.kind_of?(Array) ? e.each { |el| a << el } : a << e; a
                            end
    array.each { |el| array = array.flatten if el.kind_of?(Array) }
    array
  end
end

p ['a', ['b', 'b'], [['c', ['b']], 'a']].flatten
